#pragma once

class ShiftingPhase : public Scene
{
public:

	ifstream file;
	int numParticles;
	float scale = 1.0f;// 6.25f;
	float radius = 0.11f;

	float melting;
	float evaporation;

	string nameFile = "condensation0";

	ShiftingPhase(const char* name) : Scene(name) {
	}

	virtual void Initialize()
	{

		float minSize = 0.5f;
		float maxSize = 0.7f;

		float restDistance = radius*0.55f;
		int group = 0;

		g_numSolidParticles = g_buffers->positions.size();

		float sizex = 1.76f;
		float sizey = 2.20f;
		float sizez = 3.50f;

		int x = int(sizex / restDistance);
		int y = int(sizey / restDistance);
		int z = int(sizez / restDistance);

		numFile = 0;
		numFrames = 0;
		importFile();

		//CreateParticleGrid(Vec3(0.0f, restDistance*0.5f, 0.0f), x, y, z, restDistance, Vec3(0.0f), 1.0f, false, 0.0f, NvFlexMakePhase(group++, eNvFlexPhaseSelfCollide | eNvFlexPhaseFluid));
		initializedPosition( Vec3(0.0f, 0.0f, 0.0f), 1.0f);

		g_params.radius = radius;
		g_params.dynamicFriction = 0.0f;
		g_params.fluid = true;
		g_params.viscosity = 0.0f;
		g_params.numIterations = 1;
		g_params.vorticityConfinement = 40.f;
		g_params.anisotropyScale = 18.0f;//2.0f / radius;//25.0f;
		g_params.fluidRestDistance = restDistance;
		//g_params.numPlanes = 5;
		//g_params.cohesion = 0.05f; 


		g_maxDiffuseParticles = 128 * 1024;
		g_diffuseScale = 0.75;// 0.75f;

		g_waveFloorTilt = 0.0f;//-0.025f;

		g_lightDistance *= 100.0f;

		// draw options
		g_drawDensity = true;
		g_drawDiffuse = true;
		g_drawEllipsoids = true;
		g_drawPoints = false;

		g_warmup = true;
		
	}
	int numFrames = 0;
	int numFile = 0;
	int seconds = 10;
	int totalFrame = 64 * seconds;

	//int cont = 0;
	virtual void Update()
	{
		/*cont++;
		if ( cont != 3 ) {			
			return;
		}
		cont = 0;*/

		if (numFrames == totalFrame) {

			file.close();

			char bufPath[255];
			//sprintf(bufPath, "../../data/data%d.txt", numFile++);
			//sprintf(bufPath, "../../data/%s%d.txt",nameFile, numFile++);
			sprintf(bufPath, "C:\\movies\\%s.txt", nameFile, numFile++);
			
			string path = GetFilePathByPlatform(bufPath).c_str();
			file = ifstream(path, ios::in | ios::binary);

			if (!file) {
				g_pause = true;
				return;
			}

			float t;
			file.read((char *)(&t), sizeof(float));

			cout << t << endl;
			file.seekg(0, ios::beg);

			numFrames = 0;
		}


		
		int phase = NvFlexMakePhase(0, eNvFlexPhaseFluid);
		int phaseLiquid = NvFlexMakePhase(0, eNvFlexPhaseSelfCollide | eNvFlexPhaseFluid);
		int phaseSolid = NvFlexMakePhase(0, eNvFlexPhaseSelfCollide | eNvFlexPhaseFluid);
		int phaseGas = NvFlexMakePhase(0, eNvFlexPhaseFluid);
		float x, y, z, t;

		g_buffers->positionsFluid.destroy();
		g_buffers->positionsGas.destroy();


		std::vector<float> tempLiquid;
		std::vector<float> tempGas;
		

		int indexSolid = 0;

		for (int i = 0; i < numParticles; i++) {

			file.read((char *)(&t), sizeof(float));
			// read positions
			file.read((char *)(&x), sizeof(float));
			file.read((char *)(&y), sizeof(float));
			file.read((char *)(&z), sizeof(float));
			x *= scale; y *= scale; z *= scale;

			if (t <= 0.0f) {
				//g_buffers->positions.push_back(Vec4(x, y, z, invMass));
				g_buffers->positions[indexSolid].x = x;
				g_buffers->positions[indexSolid].y = y;
				g_buffers->positions[indexSolid].z = z;
				g_buffers->phases[indexSolid] = phaseSolid;
				g_buffers->densities[indexSolid] = t;
				indexSolid++;
			}
			else if(t<=100.0f) {
				g_buffers->positionsFluid.push_back(Vec4(x, y, z, 0));
				tempLiquid.push_back(t);
			}
			else {
				g_buffers->positionsGas.push_back(Vec4(x, y, z, 0));
				tempGas.push_back(t);
			}
			g_buffers->velocities[i].x = 0;//vx;
			g_buffers->velocities[i].y = 0;//vy;
			g_buffers->velocities[i].z = 0;//vz;

		}
		
		//*
		int numParticlesFluid = g_buffers->positionsFluid.size();
		numberParticlesSolid = indexSolid;

		for (int i = 0; i < numParticlesFluid; i++) {
			g_buffers->positions[indexSolid].x = g_buffers->positionsFluid[i].x;
			g_buffers->positions[indexSolid].y = g_buffers->positionsFluid[i].y;
			g_buffers->positions[indexSolid].z = g_buffers->positionsFluid[i].z;

			//if (tempLiquid[i]<=22)
				g_buffers->phases[indexSolid] = phaseLiquid;
			/*
			else
				g_buffers->phases[indexSolid] = phase;
			//*/

			
			g_buffers->densities[indexSolid] = tempLiquid[i];
			indexSolid++;
		}//*/		
		int numParticlesGas = g_buffers->positionsGas.size();
		numberParticlesLiquid = indexSolid;

		for (int i = 0; i < numParticlesGas; i++) {
			g_buffers->positions[indexSolid].x = g_buffers->positionsGas[i].x;
			g_buffers->positions[indexSolid].y = g_buffers->positionsGas[i].y;
			g_buffers->positions[indexSolid].z = g_buffers->positionsGas[i].z;
			g_buffers->phases[indexSolid] = phaseGas;
			g_buffers->densities[indexSolid] = tempGas[i];
			indexSolid++;
		}//*/
		numberParticlesGas = indexSolid;
		numFrames++;
	}	
	virtual void importFile() {
		if(file)
			file.close();

		char bufPath[255];
		//sprintf(bufPath, "../../data/bunny-shifting%d.txt", numFile++);
		sprintf(bufPath, "C:\\movies\\%s.txt", nameFile, numFile++);
		//sprintf(bufPath, "../../data/%s%d.txt", nameFile, numFile++);

		string path = GetFilePathByPlatform(bufPath).c_str();
		file = ifstream(path, ios::in | ios::binary );

		if (!file)
			return;

		file.read((char *)(&numParticles),sizeof(int));
		printf("number particles: %d\n", numParticles);
	}
	void initializedPosition(Vec3 velocity, float invMass){

		int phase = NvFlexMakePhase(0, eNvFlexPhaseSelfCollide | eNvFlexPhaseFluid);
		int phaseSolid = NvFlexMakePhase(0, eNvFlexPhaseSelfCollide | eNvFlexPhaseFluid);
		int phaseGas = NvFlexMakePhase(0, eNvFlexPhaseFluid);

		std::vector<float> tempLiquid;
		std::vector<float> tempGas;

		float x, y, z, t;
		for (int i = 0; i < numParticles;i++) {
			
			file.read((char *)(&t), sizeof(float));
			// read positions
			file.read((char *)(&x), sizeof(float));
			file.read((char *)(&y), sizeof(float));
			file.read((char *)(&z), sizeof(float));
			x *= scale; y *= scale; z *= scale;


			if (t <= 0.0f) {
				g_buffers->positions.push_back(Vec4(x, y, z, invMass));
				g_buffers->phases.push_back( phaseSolid );
				g_buffers->densities.push_back(t);
			}
			else if( t <= 100.0f) {
				g_buffers->positionsFluid.push_back(Vec4(x, y, z, invMass));
				tempLiquid.push_back(t);
			}
			else {
				g_buffers->positionsGas.push_back(Vec4(x, y, z, invMass));
				tempGas.push_back(t);
			}

			g_buffers->velocities.push_back(velocity);
		}

		int numParticlesFluid = g_buffers->positionsFluid.size();
		int numParticlesGas = g_buffers->positionsGas.size();
		numberParticlesSolid = g_buffers->positions.size();		
		
		for (int i = 0; i < numParticlesFluid; i++) {
			g_buffers->positions.push_back(g_buffers->positionsFluid[i]);
			g_buffers->phases.push_back( phase );
			g_buffers->densities.push_back(tempLiquid[i]);
		}
		numberParticlesLiquid = g_buffers->positions.size();

		for (int i = 0; i < numParticlesGas; i++) {
			g_buffers->positions.push_back(g_buffers->positionsGas[i]);
			g_buffers->phases.push_back(phaseGas);
			g_buffers->densities.push_back(tempGas[i]);
		}
		numberParticlesGas= g_buffers->positions.size();
		file.seekg(4, ios::beg);
	}	
};